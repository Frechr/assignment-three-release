﻿using AssignmentThree.Models;
using System.ComponentModel.DataAnnotations;

namespace AssignmentThree.Models.DTO
{
    public class FranchiseDTO
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [MaxLength(50)]
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [MaxLength(1000)]
        public string? Description { get; set; }
    }
}
