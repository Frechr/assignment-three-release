﻿using AssignmentThree.Models;
using System.ComponentModel.DataAnnotations;

namespace AssignmentThree.Models.DTO
{
    public class MovieCharactersDTO
    {
        /// <summary>
        /// A movie can have many characters.        
        /// </summary>
        /// <value>
        /// The movies.
        /// </value>
        public virtual ICollection<Character>? Characters { get; set; }
    }
}
