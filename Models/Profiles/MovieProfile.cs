﻿using AssignmentThree.Models.DTO;
using AutoMapper;

namespace AssignmentThree.Models.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieDTO>();
            CreateMap<Movie, MovieCharactersDTO>();
        }
    }
}
