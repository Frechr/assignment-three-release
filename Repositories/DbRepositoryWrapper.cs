﻿using AssignmentThree.Data;
using AssignmentThree.Repositories.Base;
using Microsoft.EntityFrameworkCore;

namespace AssignmentThree.Repositories
{
    /// <summary>
    /// The main access point to the repositories, if we want to add more repositories this makes it easier.
    /// </summary>
    /// <seealso cref="AssignmentThree.Repositories.IRepositoryWrapper" />
    public class DbRepositoryWrapper : IRepositoryWrapper
    {
        /// <summary>
        /// Gets the context.
        /// </summary>
        /// <value>
        /// The context.
        /// </value>
        private MovieDbContext _context { get; }

        /// <inheritdoc/>
        private ICharacterRepository _characterRepository;
        /// <summary>
        /// The franchise repository
        /// </summary>
        private IFranchiseRepository _franchiseRepository;
        /// <summary>
        /// The movie repository
        /// </summary>
        private IMovieRepository _movieRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="DbRepositoryWrapper"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <exception cref="System.ArgumentNullException">Failed to initialize MovieDbContext.</exception>
        public DbRepositoryWrapper(MovieDbContext context)
        {
            _context = context ?? throw new ArgumentNullException("Failed to initialize MovieDbContext.");
        }
        /// <summary>
        /// Gets the characters.
        /// </summary>
        /// <value>
        /// The characters.
        /// </value>
        public ICharacterRepository Characters
        {
            get
            {
                if (_characterRepository is null)
                {
                    _characterRepository = new DbCharacterRepository(_context);
                }
                return _characterRepository;
            }
        }

        /// <summary>
        /// Gets the franchises.
        /// </summary>
        /// <value>
        /// The franchises.
        /// </value>
        public IFranchiseRepository Franchises
        {
            get
            {
                if (_franchiseRepository is null)
                {
                    _franchiseRepository = new DbFranchiseRepository(_context);
                }
                return _franchiseRepository;
            }
        }
        /// <summary>
        /// Gets the movies.
        /// </summary>
        /// <value>
        /// The movies.
        /// </value>
        public IMovieRepository Movies
        {
            get
            {
                if (_movieRepository is null)
                {
                    _movieRepository = new DbMovieRepository(_context);
                }
                return _movieRepository;
            }
        }


        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
