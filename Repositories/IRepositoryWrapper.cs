﻿using AssignmentThree.Repositories.Base;

namespace AssignmentThree.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRepositoryWrapper 
    {
        /// <summary>
        /// Gets the characters.
        /// </summary>
        /// <value>
        /// The characters.
        /// </value>
        ICharacterRepository Characters { get; }
        /// <summary>
        /// Gets the franchises.
        /// </summary>
        /// <value>
        /// The franchises.
        /// </value>
        IFranchiseRepository Franchises { get; }
        /// <summary>
        /// Gets the movies.
        /// </summary>
        /// <value>
        /// The movies.
        /// </value>
        IMovieRepository Movies { get; }
        /// <summary>
        /// Saves this instance.
        /// </summary>
        void Save();
    }
}
