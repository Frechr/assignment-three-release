﻿using AssignmentThree.Models;

namespace AssignmentThree.Repositories.Base
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="AssignmentThree.Repositories.Base.IRepository&lt;AssignmentThree.Models.Character&gt;" />
    public interface ICharacterRepository : IRepository<Character>
    {
    }
}