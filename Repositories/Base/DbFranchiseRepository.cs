﻿using AssignmentThree.Data;
using AssignmentThree.Models;
using Microsoft.EntityFrameworkCore;

namespace AssignmentThree.Repositories.Base
{
    /// <summary>
    /// Database repository for franchises.
    /// </summary>
    /// <seealso cref="AssignmentThree.Repositories.Base.DbRepositoryBase&lt;AssignmentThree.Models.Franchise&gt;" />
    /// <seealso cref="AssignmentThree.Repositories.Base.IFranchiseRepository" />
    public class DbFranchiseRepository : DbRepositoryBase<Franchise>, IFranchiseRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DbFranchiseRepository"/> class.
        /// </summary>
        /// <param name="context"></param>
        public DbFranchiseRepository(MovieDbContext context) : base(context)
        {
        }
    }
}
