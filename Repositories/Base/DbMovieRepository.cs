﻿using AssignmentThree.Models;
using AssignmentThree.Data;
using Microsoft.EntityFrameworkCore;

namespace AssignmentThree.Repositories.Base
{
    /// <summary>
    /// Database repository for movies.
    /// </summary>
    /// <seealso cref="AssignmentThree.Repositories.Base.DbRepositoryBase&lt;AssignmentThree.Models.Movie&gt;" />
    /// <seealso cref="AssignmentThree.Repositories.Base.IMovieRepository" />
    public class DbMovieRepository : DbRepositoryBase<Movie>, IMovieRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DbMovieRepository"/> class.
        /// </summary>
        /// <param name="context"></param>
        public DbMovieRepository(MovieDbContext context) : base(context)
        {
        }
        /// <summary>
        /// Updates the movie's characters.
        /// </summary>
        /// <param name="movieId">The movie identifier.</param>
        /// <param name="characters">The characters.</param>
        /// <returns></returns>
        public bool UpdateCharacters(int movieId, List<Character> characters)
        {
            var movieToUpdate = Get(movieId);
            repositoryContext.Add(movieToUpdate);
            repositoryContext.Add(characters);
            movieToUpdate.Characters = characters;
            return Update(movieToUpdate);
        }
    }
}
