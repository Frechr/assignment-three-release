﻿using AssignmentThree.Data;
using AssignmentThree.Models;
using Microsoft.EntityFrameworkCore;

namespace AssignmentThree.Repositories.Base
{
    /// <summary>
    /// Database repository for characters.
    /// </summary>
    /// <seealso cref="AssignmentThree.Repositories.Base.DbRepositoryBase&lt;AssignmentThree.Models.Character&gt;" />
    /// <seealso cref="AssignmentThree.Repositories.Base.ICharacterRepository" />
    public class DbCharacterRepository : DbRepositoryBase<Character>, ICharacterRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DbCharacterRepository"/> class.
        /// </summary>
        /// <param name="context"></param>
        public DbCharacterRepository(MovieDbContext context) : base(context)
        {

        }
    }
}
