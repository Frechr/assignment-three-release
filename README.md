
# Assignment 3 #

## Project description ##

This repository consists of a solution to an assignment in participation of a .NET course.

The assignment was split in 2 parts...

Appendix A:
The task is to create a datastore and RESTful API for storing and manipulating movie characters using ASP.NET Core and EF Core with a SQL Server database. 
The database will store information about characters, movies, and franchises. 
	
Appendix B:
The API must have full CRUD operations for movies, characters, and franchises, including endpoints for updating related data. 
Reports for all entities should also be provided. 
The API should use DTOs with AutoMapper, have proper documentation with Swagger, and use Services or Repositories for database manipulation. 
The API will be deployed as a Docker artifact using a CI pipeline.


## Installation ##

You can install this project by using git clone with either ssh or https.


## Usage ##

To use this you can run the application through Visual studio and watch the Swagger documentation explaining each endpoint.

## Contributors ##

Jacob Emil Tornes, Fredrik Christensen
