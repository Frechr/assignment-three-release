﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AssignmentThree.Data;
using AssignmentThree.Models;
using AssignmentThree.Repositories;
using AutoMapper;
using AssignmentThree.Models.DTO;
using System.Net.Mime;

namespace AssignmentThree.Controllers
{
    /// <summary>
    /// Web API endpoint for Franchises.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        /// <summary>
        /// The context
        /// </summary>
        private readonly IRepositoryWrapper _context;
        /// <summary>
        /// The mapper
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="FranchisesController"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="mapper">The mapper.</param>
        public FranchisesController(IRepositoryWrapper context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Franchises
        /// <summary>
        /// Gets the franchises.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<IEnumerable<FranchiseDTO>> GetFranchises()
        {
            var franchises = _context.Franchises.GetAll()?.ToList() ?? new List<Franchise>();
            return franchises.Select(franchise=> _mapper.Map<FranchiseDTO>(franchise)).ToList();
        }

        /// <summary>
        /// Gets the franchise.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ActionResult<FranchiseDTO> GetFranchise(int id)
        {
            var franchise = _context.Franchises.Get(id);

            if (franchise is null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseDTO>(franchise);
        }

        /// <summary>
        /// Puts the franchise.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="franchise">The franchise.</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult PutFranchise(int id, FranchiseDTO franchise)
        {
            if(!FranchiseExists(id))
                return NotFound();
            if (franchise is null)
            {
                return BadRequest();
            }
            _context.Franchises.Update(_mapper.Map<Franchise>(franchise));

            return NoContent();
        }

        /// <summary>
        /// Puts the franchise movies.
        /// </summary>
        /// <param name="franchiseId">The franchise identifier.</param>
        /// <param name="movieIdentifiers">The movie identifiers.</param>
        /// <returns></returns>
        [HttpPut("{id}/movies")]
        public IActionResult PutFranchiseMovies(int franchiseId, int[] movieIdentifiers)
        {
            if (!FranchiseExists(franchiseId))
                return NotFound();
            if (movieIdentifiers is null)
                return BadRequest();
            var franchise = _context.Franchises.Get(franchiseId);
            var newMovies = _context.Movies.GetAll().Where(movie => movieIdentifiers.Contains(movie.Id)).ToList();
            franchise.Movies = newMovies;
            _context.Save();
            return NoContent();
        }

        // POST: api/Franchises
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Posts the franchise.
        /// </summary>
        /// <param name="franchise">The franchise.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<FranchiseDTO> PostFranchise(FranchiseDTO franchise)
        {
            _context.Franchises.Create(_mapper.Map<Franchise>(franchise)); 

            return CreatedAtAction("GetFranchise", franchise);
        }

        // DELETE: api/Franchises/5
        /// <summary>
        /// Deletes the franchise.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult DeleteFranchise(int id)
        {
            var franchise = _context.Franchises.Get(id);
            if (franchise == null)
            {
                return NotFound();
            }
            _context.Franchises.Delete(franchise); 
            return NoContent();
        }

        /// <summary>
        /// Franchises the exists.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Get(id) is not null;
        }
    }
}
