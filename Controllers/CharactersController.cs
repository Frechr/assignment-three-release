﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AssignmentThree.Models;
using AssignmentThree.Data;
using AssignmentThree.Repositories;
using AutoMapper;
using AssignmentThree.Models.DTO;
using System.Net.Mime;

namespace AssignmentThree.Controllers
{
    /// <summary>
    /// Web API endpoint for Characters.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        /// <summary>
        /// The context
        /// </summary>
        private readonly IRepositoryWrapper _context;
        /// <summary>
        /// The mapper
        /// </summary>
        private readonly IMapper _mapper;
        /// <summary>
        /// Initializes a new instance of the <see cref="CharactersController"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="mapper">The mapper.</param>
        public CharactersController(IRepositoryWrapper context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Characters
        /// <summary>
        /// Gets the characters.
        /// </summary>
        /// <returns></returns>
        [HttpGet] 
        public ActionResult<IEnumerable<CharacterDTO>> GetCharacters()
        {
            var characters = _context.Characters.GetAll();
            var charactersDTO = characters.Select(character => _mapper.Map<CharacterDTO>(character));
            return charactersDTO.ToList();
        }

        // GET: api/Characters/5
        /// <summary>
        /// Gets the character.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>A characterDTO.</returns>
        [HttpGet("{id}")] 
        public ActionResult<CharacterDTO> GetCharacter(int id)
        {
            var character = _context.Characters.Get(id);

            if (character is null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterDTO>(character);
        }

        // PUT: api/Characters/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Puts the character.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="character">The character.</param>
        /// <returns>Action result.</returns>
        [HttpPut("{id}")] 
        public IActionResult PutCharacter(int id, CharacterDTO character)
        {
            if (!CharacterExists(id))
            {
                return NotFound();
            }
            if ( character is null)
            {
                return BadRequest();
            }
            _context.Characters.Update(_mapper.Map<Character>(character));

            return NoContent();
        }

        // POST: api/Characters
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Posts the character.
        /// </summary>
        /// <param name="character">The character.</param>
        /// <returns>The characterDTO.</returns>
        [HttpPost] 
        public ActionResult<CharacterDTO> PostCharacter(CharacterDTO character)
        {
            if (character is null)
            {
                return BadRequest("Character is null.");
            }
            _context.Characters.Create(_mapper.Map<Character>(character));

            return CreatedAtAction("GetCharacter", character);
        }

        // DELETE: api/Characters/5
        /// <summary>
        /// Deletes the character.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The action result.</returns>
        [HttpDelete("{id}")] 
        public IActionResult DeleteCharacter(int id)
        {
            var character = _context.Characters.Get(id);
            if (!CharacterExists(id))
            {
                return NotFound();
            }
            _context.Characters.Delete(character);
            return NoContent();
        }

        /// <summary>
        /// Characters the exists.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Wether character exists or not.</returns>
        private bool CharacterExists(int id)
        {
            return _context.Characters.Get(id) is not null;
        }
    }
}
